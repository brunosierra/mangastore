<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('pedidos/boleto/{id}', 'PedidosController@boleto');
Route::get('pedidos/debit/{id}', 'PedidosController@debit');
Route::get('pedidos/card/{id}', 'PedidosController@card');

Route::resource('pedidos', 'PedidosController');
Route::get('/', function () {
    return redirect()->action('PedidosController@index');
});
