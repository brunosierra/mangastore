<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Meus pedidos</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="css/app.css" rel="stylesheet" type="text/css">
        
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
        <script type="text/javascript">
        window.sessionId = "{{$sessionPagSeguro}}";
        PagSeguroDirectPayment.setSessionId(window.sessionId);
        </script>
    </head>
    <body>
        
        <header class="layout-header">
            <section class="container">
                <a href="tel:13988619523" class="layout-contact">(13) 98861-9523</a>
                
                <ul class="layout-perfil">
                    <li>
                        <a href="">Olá, Bruno Alves</a>
                        <ul>
                            <li><a href="">Github</a></li>
                            <li><a href="">Site</a></li>
                            <li><a href="">Facebook</a></li>
                        </ul>
                    </li>
                </ul>
            </section>
            
            <section class="layout-brand">
                <a href="#home">PHPJob Agência Manga</a>
            </section>
        </header>
        
        <section class="container">
            <div class="row">
                <div class="col-md-12">
                        
                    <h1># LISTA DE PEDIDOS / Pagina {{$orders->currentPage()}} de {{$orders->lastPage()}}</h1>
                    
                    @foreach($orders as $order)
                    <section class="orders row">
                        
                        <div class="orders-primary col-md-4">
                            <dl>
                                <dt>Pedido</dt>
                                <dd>#{{$order->id}}</dd>
                                <dt>Valor</dt>
                                <dd>@toBRL($order->total)</dd>
                            </dl>
                        </div>
                        <div class="orders-information col-md-4">
                            <dl>
                                <dt>Situação do pedido</dt>
                                <dd>{{$order->status}}</dd>
                            </dl>
                        </div>
                        <div class="orders-more col-md-4">
                            <a href="#more-{{$order->id}}" class="openMore">Mais detalhes</a>
                        </div>
                        
                        <div id="more-{{$order->id}}" class="order-more-information">
                            <fieldset>
                                <legend>Mais detalhes do pedido</legend>
                                <div class="row">
                                    <div class="orders-pay col-md-4">
                                        <strong>Forma de pagamento</strong>
                                        <div class="orders-button">
                                            <a href="#pagamento-{{$order->id}}" data-price="{{$order->total}}" class="initPay">Pagar com Pagseguro</a>
                                        </div>
                                    </div>
                                    <div class="orders-price col-md-4">
                                        <strong>Total pago</strong>
                                        <dl>
                                            <dt>Subtotal</dt>
                                            <dd>@toBRL($order->subtotal)</dd>
                                            <dt>Frete</dt>
                                            <dd>@toBRL($order->frete)</dd>
                                            <dt class="last">Total</dt>
                                            <dd class="last">@toBRL($order->total)</dd>
                                        </dl>
                                    </div>
                                    <div class="orders-delivery col-md-4">
                                        <strong>Endereço de entrega</strong>
                                        <address>
                                            <strong>{{$order->entrega->nome_destinatario}}</strong><br/>
                                            {{$order->entrega->endereco}}, Nº {{$order->entrega->numero}}<br/>{{$order->entrega->bairro}}, {{$order->entrega->cidade}}/{{$order->entrega->estado}}
                                            <br/>
                                            CEP: {{$order->entrega->cep}}<br/>Ref: {{$order->entrega->referencia}}
                                        </address>
                                        <dl>
                                            <dt>Prazo de entrega</dt>
                                            <dd>{{$order->prazo_entrega}}</dd>
                                        </dl>
                                    </div>
                                </div>
                                <div class="row">
                                    <table class="table table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th># Produto</th>
                                                <th>Unidades</th>
                                                <th>Valor Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($order->itens as $item)
                                            <tr>
                                                <td>{{$item->produto->nome}}</td>
                                                <td>{{$item->quantidade}}</td>
                                                <td>@toBRL($item->valor_total)</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </section>
                    @endforeach
                    
                    <div id="myModal" class="modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Formas de pagamento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body row" id="pageModal">
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="loader-wrapper">
                        <div id="loader"></div>
                        <div class="loader-section"></div>
                    </div>
                    
                    <template id="creditCard">
                        <div class="row layout-credit-card">
                        <h2>Cartão: #brand</h1>
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="card-holder-name">Nome no cartão</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="card-holder-name" id="card-holder-name" placeholder="Escreva igual ao cartão" value="Jose Comprador">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="card-number">Nº do cartão</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="card-number" id="card-number" placeholder="Número do cartão Débito/Crédito" value="4111111111111111">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="expiry-month">Expiração</label>
                            <div class="col-sm-9">
                              <div class="row">
                                <div class="col-xs-3">
                                  <select class="form-control col-sm-2" name="expiry-month" id="expiry-month">
                                    <option>Mês</option>
                                    <option value="01">Jan (01)</option>
                                    <option value="02">Fev (02)</option>
                                    <option value="03">Mar (03)</option>
                                    <option value="04">Abr (04)</option>
                                    <option value="05">Mai (05)</option>
                                    <option value="06">Jun (06)</option>
                                    <option value="07">Jul (07)</option>
                                    <option value="08">Ago (08)</option>
                                    <option value="09">Set (09)</option>
                                    <option value="10">Out (10)</option>
                                    <option value="11">Nov (11)</option>
                                    <option value="12" selected>Dez (12)</option>
                                  </select>
                                </div>
                                <div class="col-xs-3">
                                  <select class="form-control" name="expiry-year" id="expiry-year">
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                    <option value="2029">2029</option>
                                    <option value="2030" selected>2030</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="cvv">Código CVV</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="cvv" id="cvv" placeholder="Security Code" value="123">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                              <button type="button" class="btn btn-success btn-pay-credit-card">Pagar</button>
                            </div>
                          </div>
                        </div>
                    </template>
                    
                </div>
                
            </div>
            <div class="row layout-pagination">
                <div class="col-md-12">
                {{$orders->links()}}
                </div>
            </div>
        </section>
        
        <footer class="layout-footer">
            <section class="container">
                <span class="layout-address">São Vicente/SP</span>
                <span class="layout-brand">Manga</span>
            </div>
        </footer>
        
    </body>
</html>
