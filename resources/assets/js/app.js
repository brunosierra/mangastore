
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready( () => {
    
    $('.openMore').click(function(event){
        event.isPropagationStopped()
        
        let alvo = $( $(this).attr('href') );
        
        if(alvo.css('display')=='none'){
            $(this).html('Menos detalhes');
            alvo.slideDown('slow');
            return false;
        }
        
        $(this).html('Mais detalhes');
        alvo.slideUp('slow');
        
        return false;
    });
    
    $('.initPay').click(function(event){
        event.isPropagationStopped()
        
        let _this = this;
        let alvo = $(_this).attr('href').replace(/\D/g, '');
        let prince = $(_this).attr('data-price').replace(/[^\d\.]/g, '');
        let textDefault = $(_this).html();
        alert(prince);
        $(_this).html("Aguarde...");
        PagSeguroDirectPayment.getPaymentMethods({
            amount: prince,
            success: function(response) {
                let htmlPageModal = '';                
                $("#pageModal").empty();
                
                if(response.paymentMethods.hasOwnProperty("BOLETO")){
                    let htmlDivDebit = $('<div>');
                        htmlDivDebit.attr('class', 'layout-options');
                        htmlDivDebit.append( $('<h2>').html('Boleto') );
                    
                    facade = response.paymentMethods.BOLETO.options;
                    for(BOLETO in facade){
                        let htmlImg = $('<img>');
                            htmlImg.attr('src', 'https://stc.pagseguro.uol.com.br'+facade[BOLETO].images.MEDIUM.path);
                        
                        let htmlA = $('<a>');
                            htmlA.attr('href', '#BOLETO');
                            htmlA.attr('class', 'layout-options-pay layout-options-pay-boleto');
                            htmlA.attr('title', facade[BOLETO].displayName);
                            htmlA.attr('data-order', alvo);
                            htmlA.attr('data-method', BOLETO);
                            htmlA.attr('data-name', facade[BOLETO].displayName);
                            htmlA.attr('data-price', prince);
                            htmlA.html(htmlImg);
                            
                        htmlDivDebit.append( htmlA );
                    }
                    
                    $("#pageModal").append(htmlDivDebit);
                }
                
                if(response.paymentMethods.hasOwnProperty("CREDIT_CARD")){
                    let htmlDivDebit = $('<div>');
                        htmlDivDebit.attr('class', 'layout-options');
                        htmlDivDebit.append( $('<h2>').html('Débito em conta') );
                    
                    facade = response.paymentMethods.CREDIT_CARD.options;
                    for(CREDIT_CARD in facade){
                        let htmlImg = $('<img>');
                            htmlImg.attr('src', 'https://stc.pagseguro.uol.com.br'+facade[CREDIT_CARD].images.MEDIUM.path);
                        
                        let htmlA = $('<a>');
                            htmlA.attr('href', '#CREDIT_CARD');
                            htmlA.attr('class', 'layout-options-pay layout-options-pay-credit_card');
                            htmlA.attr('title', facade[CREDIT_CARD].displayName);
                            htmlA.attr('data-order', alvo);
                            htmlA.attr('data-method', CREDIT_CARD);
                            htmlA.attr('data-name', facade[CREDIT_CARD].displayName);
                            htmlA.attr('data-price', prince);
                            htmlA.html(htmlImg);
                            
                        htmlDivDebit.append( htmlA );
                    }
                    
                    $("#pageModal").append(htmlDivDebit);
                        
                }
                
                if(response.paymentMethods.hasOwnProperty("ONLINE_DEBIT")){
                    let htmlDivDebit = $('<div>');
                        htmlDivDebit.attr('class', 'layout-options');
                        htmlDivDebit.append( $('<h2>').html('Débito em conta') );
                    
                    facade = response.paymentMethods.ONLINE_DEBIT.options;
                    for(ONLINE_DEBIT in facade){
                        let htmlImg = $('<img>');
                            htmlImg.attr('src', 'https://stc.pagseguro.uol.com.br'+facade[ONLINE_DEBIT].images.MEDIUM.path);
                        
                        let htmlA = $('<a>');
                            htmlA.attr('href', '#ONLINE_DEBIT');
                            htmlA.attr('class', 'layout-options-pay layout-options-pay-online_debit');
                            htmlA.attr('title', facade[ONLINE_DEBIT].displayName);
                            htmlA.attr('data-order', alvo);
                            htmlA.attr('data-method', ONLINE_DEBIT);
                            htmlA.attr('data-name', facade[ONLINE_DEBIT].displayName);
                            htmlA.attr('data-price', prince);
                            htmlA.html(htmlImg);
                            
                        htmlDivDebit.append( htmlA );
                    }
                    
                    $("#pageModal").append(htmlDivDebit);
                }
                
                //$("#pageModal").html(htmlPageModal);
                $('#myModal').modal('show')
            },
            error: function(response) {
                alert("Não foi possivel recuperar as formas de pagamento!");
            },
            complete: function(response) {
                $(_this).html(textDefault);
            }
        });
        
        return false;
    });
    
    // Pagamentos debitos online
    $('#myModal').on('click', '.layout-options-pay-online_debit', function(event){
        $('BODY').removeClass('loaded').addClass('loading');
        
        let order = $(this).attr('data-order');
        let method = $(this).attr('data-method');
        
        axios.get('/pedidos/debit/' +order +'?method='+method)
            .then(function(response){
                let htmlDivDebit = $('<div>');
                    htmlDivDebit.append( $('<h2>').html('Opção escolhida: Débito em conta') );
                    htmlDivDebit.attr('style', 'text-align: center');
                
                let htmlA = $('<a>');
                    htmlA.attr('href', response.data.debit);
                    htmlA.attr('target', '_blank');
                    htmlA.attr('style', 'margin:10px auto');
                    htmlA.attr('class', 'btn btn-small btn-primary');
                    htmlA.attr('title', 'Clique aqui para acessar a página do seu banco');
                    htmlA.html('Clique aqui para gerar o boleto');
                
                htmlDivDebit.append(htmlA);
                finalizar(htmlDivDebit)
                console.log(response.status); // ex.: 200
            });  
        
    });
    
    // Pagamentos boleto online
    $('#myModal').on('click', '.layout-options-pay-boleto', function(event){
        $('BODY').removeClass('loaded').addClass('loading');
        
        let order = $(this).attr('data-order');
        let method = $(this).attr('data-method');
        
        axios.get('/pedidos/boleto/' +order +'?method='+method)
            .then(function(response){
                let htmlDivDebit = $('<div>');
                    htmlDivDebit.append( $('<h2>').html('Opção escolhida: Boleto') );
                    htmlDivDebit.attr('style', 'text-align: center');
                
                let htmlA = $('<a>');
                    htmlA.attr('href', response.data.boleto);
                    htmlA.attr('target', '_blank');
                    htmlA.attr('style', 'margin:10px auto');
                    htmlA.attr('class', 'btn btn-small btn-primary');
                    htmlA.attr('title', 'Clique aqui para gerar o boleto');
                    htmlA.html('Clique aqui para gerar o boleto');
                
                htmlDivDebit.append(htmlA);
                finalizar(htmlDivDebit)
                console.log(response.status); // ex.: 200
            });  
        
    });
    
    // Pagamentos boleto online
    $('#myModal').on('click', '.layout-options-pay-credit_card', function(event){
        
        window._order = $(this).attr('data-order');
        window._method = $(this).attr('data-method');
        window._name = $(this).attr('data-name');
        window._price = $(this).attr('data-price');
        
        $('#pageModal').html( $('#creditCard').html().replace(/#brand/, window._name) );
        
    });
    
    // Pagamentos boleto online
    $('#myModal').on('click', '.btn-pay-credit-card', function(event){
        $('BODY').removeClass('loaded').addClass('loading');
        
        let order = window._order;
        let method = window._method.toLowerCase();
        let price = window._price;
        
        //alert( order +'<>'+ method +'<>'+ price);
        
        var param = {
            brand: method,
            cardNumber: $("input#card-number").val(),
            cvv: $("input#cvv").val(),
            expirationMonth: $("select#expiry-month").val(),
            expirationYear: $("select#expiry-year").val(),
            success: function(credicardResponse) {
                
                PagSeguroDirectPayment.getInstallments({
                    amount: price,
                    brand: method,
                    maxInstallmentNoInterest: 1,
                    success: function(installmentResponse) {
                    console.log(installmentResponse);
                        axios.get('/pedidos/card/' +order +'?method='+method+'&senderHash='+window.sendHashPagseguro+ '&creditCardToken='+credicardResponse.card.token+ '&installmentAmount='+installmentResponse.installments[method][0].installmentAmount+ '&totalAmount='+installmentResponse.installments[method][0].totalAmount)
                        .then(function(response){
                            let htmlDivDebit = $('<div>');
                                htmlDivDebit.append( $('<h2>').html('Opção escolhida: Cartão') );
                                htmlDivDebit.attr('style', 'text-align: center');
                            
                            let htmlA = $('<a>');
                                htmlA.attr('href', response.data.card);
                                htmlA.attr('target', '_blank');
                                htmlA.attr('style', 'margin:10px auto');
                                htmlA.attr('class', 'btn btn-small btn-primary');
                                htmlA.attr('title', 'Clique aqui para acessar a página de confirmação');
                                htmlA.html('Clique aqui para acessar a página de confirmação');
                            
                            htmlDivDebit.append(htmlA);
                            finalizar(htmlDivDebit)
                            console.log(response.status); // ex.: 200
                        });  
                    },
                    error: function(response) {
                        //tratamento do erro
                    },
                    complete: function(response) {
                        //tratamento comum para todas chamadas
                    }
                });
            },
            error: function(response) {
                alert(JSON.stringify(response))
                alert('erro');
            },
            complete: function(response) {
                //tratamento comum para todas chamadas
            }
        }
        
        //console.table(param);
        PagSeguroDirectPayment.setSessionId(window.sessionId);
        window.sendHashPagseguro = PagSeguroDirectPayment.getSenderHash();
        PagSeguroDirectPayment.createCardToken(param);
        
    });
    
    function finalizar(html){
        $("#pageModal").html(html);
        $('BODY').removeClass('loading').addClass('loaded');
    }
});