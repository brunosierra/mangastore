<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Pedidos;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Pedidos::where("status", "APROVADO")->paginate(50);        
        return view('pedidos', ['orders'=>$orders, 'sessionPagSeguro'=>sessionPagseguro()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function debit(Request $request, $id)
    {
        //
        $order = Pedidos::find($id);
        $method = $request->input('method');        
        
        return response()->json(['order'=>$order, 'method'=>$method, 'debit'=>debitPagseguro($order, $method)]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function boleto(Request $request, $id)
    {
        //
        $order = Pedidos::find($id);
        $method = $request->input('method');        
        
        return response()->json(['order'=>$order, 'method'=>$method, 'boleto'=>boletoPagseguro($order)]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function card(Request $request, $id)
    {
        //
        $order = Pedidos::find($id);
        $method = $request->input('method');  
        $hash = $request->input('senderHash');  
        $token = $request->input('creditCardToken');    
        $installmentAmount = $request->input('installmentAmount');          
        $totalAmount = $request->input('totalAmount');          
        
        return response()->json(['order'=>$order, 'method'=>$method, 'card'=>cardPagseguro($order, $hash, $token, $installmentAmount, $totalAmount)]);
    }

}
