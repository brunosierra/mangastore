<?PHP
/**
* Recupera uma sessão do pagseguro, ou cria uma nova caso não exista
*
* @param void
* @param $currency
*/
function sessionPagseguro()
{
    $postdata = http_build_query(
        array(
            'email' => 'suporte@lojamodelo.com.br',
            'token' => '57BE455F4EC148E5A54D9BB91C5AC12C',
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            //'content' => $postdata
        )
    );

    $context = stream_context_create($opts);
    $sessionPagSeguro = @file_get_contents('https://ws.sandbox.pagseguro.uol.com.br/v2/sessions?'.$postdata, false, $context);
    if(preg_match("@<id>([^<]+)<\/id>@",$sessionPagSeguro, $matches)){
        $sessionPagSeguro = $matches[1];
    }
    
    return $sessionPagSeguro;
}

/**
* Recupera a url de um boleto
* @param App\Http\Models\Pedidos $order a classe com o pedido
* @param string $method Qual a forma de debito escolhido
*/
function debitPagseguro($order, $method)
{
    $sessionPagSeguro = sessionPagseguro();
    
    // bug dos nomes
    $name = explode(' ', $order->entrega->nome_destinatario);
    if(count($name) == 1 ) {
        $name[] = ' - Não preenchido';
    }
    $name = implode(' ', $name);
        
    $senderPagSeguro = [
        // Dados básicos
        'email' => 'suporte@lojamodelo.com.br',
        'token' => '57BE455F4EC148E5A54D9BB91C5AC12C',
        
        // Dados da transação
        'paymentMode' => 'default',
        'paymentMethod' => 'eft',
        'bankName' => $method,
        'receiverEmail' => 'suporte@lojamodelo.com.br',
        'notificationURL' => 'https://sualoja.com.br/notifica.html',
        'reference' => "REF{$order->id}",
        'senderName' => $name,
        'senderCPF' => '11475714734',
        'senderAreaCode' => '99',
        'senderPhone' => '99999999',
        'senderEmail' => 'carlos@sandbox.pagseguro.com.br',
        'shippingAddressStreet' => $order->entrega->endereco,
        'shippingAddressNumber' => $order->entrega->numero,
        'shippingAddressComplement' => $order->entrega->complemento,
        'shippingAddressDistrict' => $order->entrega->bairro,
        'shippingAddressPostalCode' => $order->entrega->cep,
        'shippingAddressCity' => $order->entrega->cidade,
        'shippingAddressState' => $order->entrega->estado,
        'shippingAddressCountry' => 'BRA',
        'currency' => 'BRL',
        'extraAmount' => "{$order->total}",
        'shippingType' => '1',
        'shippingCost' => "{$order->frete}",
    ];

    // Adiciona os itens da compra
    /*foreach($order->itens as $indice => $item) {
        $index = ($indice+1);
        
        $senderPagSeguro["itemId{$index}"] = $item->id;
        $senderPagSeguro["itemDescription{$index}"] = $item->produto->nome;
        $senderPagSeguro["itemAmount{$index}"] = $item->valor_total;
        $senderPagSeguro["itemQuantity{$index}"] = $item->quantidade;
    }*/
    
    // Apenas para pode finalizar
    $senderPagSeguro["itemId1"] = 1;
    $senderPagSeguro["itemDescription1"] = "Pedido pago com débito";
    $senderPagSeguro["itemAmount1"] = "{$order->total}";
    $senderPagSeguro["itemQuantity1"] = 1;
    
    $postdata = http_build_query($senderPagSeguro);
    
    $opts = array('http' =>
        array(
            'header' => "Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1\r\n".
                        "Content-Length: ".strlen($postdata)."\r\n".
                        "User-Agent:MyAgent/1.0\r\n",
            'method' => "POST",
            'content' => $postdata,
            'ignore_errors' => true,
        )
    );
    
    $context = stream_context_create($opts);
    $resultadoPagSeguro = @file_get_contents('https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/', false, $context);

    if(preg_match("@<paymentLink>([^<]+)<\/paymentLink>@", $resultadoPagSeguro, $matches)){
        return $matches[1];
    }
    
    if(preg_match("@<message>([^<]+)<\/message>@", $resultadoPagSeguro, $matches)){
        throw new Exception($matches[1]);
    }
    
    throw new Exception("Problemas com a API");
}
    
    
/**
* Recupera a url de um boleto
* @param App\Http\Models\Pedidos $order a classe com o pedido
* @param string $method Qual a forma de debito escolhido
*/
function boletoPagseguro($order)
{
    $sessionPagSeguro = sessionPagseguro();
    
    // bug dos nomes
    $name = explode(' ', $order->entrega->nome_destinatario);
    if(count($name) == 1 ) {
        $name[] = ' - Não preenchido';
    }
    $name = implode(' ', $name);
    
    $senderPagSeguro = [
        // Dados básicos
        'email' => 'suporte@lojamodelo.com.br',
        'token' => '57BE455F4EC148E5A54D9BB91C5AC12C',
        
        // Dados da transação
        'paymentMode' => 'default',
        'paymentMethod' => 'boleto',
        'receiverEmail' => 'suporte@lojamodelo.com.br',
        'notificationURL' => 'https://sualoja.com.br/notifica.html',
        'reference' => "REF{$order->id}",
        'senderName' => $name,
        'senderCPF' => '11475714734',
        'senderAreaCode' => '99',
        'senderPhone' => '99999999',
        'senderEmail' => 'carlos@sandbox.pagseguro.com.br',
        'shippingAddressStreet' => $order->entrega->endereco,
        'shippingAddressNumber' => $order->entrega->numero,
        'shippingAddressComplement' => $order->entrega->complemento,
        'shippingAddressDistrict' => $order->entrega->bairro,
        'shippingAddressPostalCode' => $order->entrega->cep,
        'shippingAddressCity' => $order->entrega->cidade,
        'shippingAddressState' => $order->entrega->estado,
        'shippingAddressCountry' => 'BRA',
        'currency' => 'BRL',
        'extraAmount' => "{$order->total}",
        'shippingType' => '1',
        'shippingCost' => "{$order->frete}",
    ];

    // Adiciona os itens da compra
    /*foreach($order->itens as $indice => $item) {
        $index = ($indice+1);
        
        $senderPagSeguro["itemId{$index}"] = $item->id;
        $senderPagSeguro["itemDescription{$index}"] = $item->produto->nome;
        $senderPagSeguro["itemAmount{$index}"] = $item->valor_total;
        $senderPagSeguro["itemQuantity{$index}"] = $item->quantidade;
    }*/
    
    // Apenas para pode finalizar
    $senderPagSeguro["itemId1"] = 1;
    $senderPagSeguro["itemDescription1"] = "Pedido pago com boleto";
    $senderPagSeguro["itemAmount1"] = "{$order->total}";
    $senderPagSeguro["itemQuantity1"] = 1;
    
    $postdata = http_build_query($senderPagSeguro);
    
    $opts = array('http' =>
        array(
            'header' => "Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1\r\n".
                        "Content-Length: ".strlen($postdata)."\r\n".
                        "User-Agent:MyAgent/1.0\r\n",
            'method' => "POST",
            'content' => $postdata,
            'ignore_errors' => true,
        )
    );
    
    $context = stream_context_create($opts);
    $resultadoPagSeguro = @file_get_contents('https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/', false, $context);

    if(preg_match("@<paymentLink>([^<]+)<\/paymentLink>@", $resultadoPagSeguro, $matches)){
        return $matches[1];
    }
    
    if(preg_match("@<message>([^<]+)<\/message>@", $resultadoPagSeguro, $matches)){
        throw new Exception($matches[1]);
    }
    
    throw new Exception("Problemas com a API");
}

/**
* Recupera a o sucesso de um checkout com cartão
* @param App\Http\Models\Pedidos $order a classe com o pedido
* @param string $method Qual a forma de debito escolhido
*/
function cardPagseguro($order, $hash, $token, $installmentAmount, $totalAmount)
{
    $sessionPagSeguro = sessionPagseguro();
    
    // bug dos nomes
    $name = explode(' ', $order->entrega->nome_destinatario);
    if(count($name) == 1 ) {
        $name[] = ' - Não preenchido';
    }
    $name = implode(' ', $name);
    
    $senderPagSeguro = [
        // Dados básicos
        'email' => 'suporte@lojamodelo.com.br',
        'token' => '57BE455F4EC148E5A54D9BB91C5AC12C',
        
        // Dados da transação
        'paymentMode' => 'default',
        'paymentMethod' => 'creditCard',
        'receiverEmail' => 'suporte@lojamodelo.com.br',
        'notificationURL' => 'https://sualoja.com.br/notifica.html',
        'reference' => "REF{$order->id}",
        'senderName' => $name,
        'senderCPF' => '11475714734',
        'senderAreaCode' => '99',
        'senderPhone' => '99999999',
        'senderEmail' => 'carlos@sandbox.pagseguro.com.br',
        'shippingAddressStreet' => $order->entrega->endereco,
        'shippingAddressNumber' => $order->entrega->numero,
        'shippingAddressComplement' => $order->entrega->complemento,
        'shippingAddressDistrict' => $order->entrega->bairro,
        'shippingAddressPostalCode' => $order->entrega->cep,
        'shippingAddressCity' => $order->entrega->cidade,
        'shippingAddressState' => strtoupper($order->entrega->estado),
        'shippingAddressCountry' => 'BRA',
        'currency' => 'BRL',
        'extraAmount' => "0.00",
        'shippingType' => '1',
        'shippingCost' => "{$order->frete}",
        
        'senderHash' => $hash,
        'creditCardToken' => $token,
        'installmentQuantity' => '1',
        'installmentValue' => 0.0,
        //'noInterestInstallmentQuantity' => '1',
        'creditCardHolderName' => 'Jose Comprador',
        'creditCardHolderCPF' => '11475714734',
        'creditCardHolderBirthDate' => '01/01/1900',
        'creditCardHolderAreaCode' => '99',
        'creditCardHolderPhone' => '99999999',
        'billingAddressStreet' => $order->entrega->endereco,
        'billingAddressNumber' => $order->entrega->numero,
        'billingAddressComplement' => $order->entrega->complemento,
        'billingAddressDistrict' => $order->entrega->bairro,
        'billingAddressPostalCode' => $order->entrega->cep,
        'billingAddressCity' => $order->entrega->cidade,
        'billingAddressState' => strtoupper($order->entrega->estado),
        'billingAddressCountry' => 'BRA',
    ];

    // Adiciona os itens da compra
    /*foreach($order->itens as $indice => $item) {
        $index = ($indice+1);
        
        $senderPagSeguro["itemId{$index}"] = $item->id;
        $senderPagSeguro["itemDescription{$index}"] = $item->produto->nome;
        $senderPagSeguro["itemAmount{$index}"] = $item->valor_total;
        $senderPagSeguro["itemQuantity{$index}"] = $item->quantidade;
    }*/
    
    // Apenas para pode finalizar
    $senderPagSeguro["itemId1"] = 1;
    $senderPagSeguro["itemDescription1"] = "Pedido pago com cartão";
    $senderPagSeguro["itemAmount1"] = number_format($installmentAmount, 2, ".", "");
    $senderPagSeguro["itemQuantity1"] = 1;
    
    $senderPagSeguro["installmentValue"] = number_format($installmentAmount, 2, ".", ""); 
    $postdata = http_build_query($senderPagSeguro);
    
    $opts = array('http' =>
        array(
            'header' => "Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1\r\n".
                        "Content-Length: ".strlen($postdata)."\r\n".
                        "User-Agent:MyAgent/1.0\r\n",
            'method' => "POST",
            'content' => $postdata,
            'ignore_errors' => true,
        )
    );
    
    $context = stream_context_create($opts);
    $resultadoPagSeguro = file_get_contents('https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/', false, $context);

    if(preg_match("@<paymentLink>([^<]+)<\/paymentLink>@", $resultadoPagSeguro, $matches)){
        return $matches[1];
    }
    
    if(preg_match("@<message>([^<]+)<\/message>@", $resultadoPagSeguro, $matches)){
        throw new Exception($matches[1]);
    }
    
    throw new Exception("Problemas com a API");
}