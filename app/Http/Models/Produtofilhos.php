<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Produtofilhos extends Model
{
    // Tabela anexa
    protected $table = 'produto_filho';
    
    /**
     * Relacionamento hasOne
     * produto
     */
    public function itens(){
        return $this->hasMany('App\Http\Models\Itens', 'id_produto', 'id');
    }
}
