<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Entregas extends Model
{
    // Tabela anexa
    protected $table = 'pedido_endereco_entrega';
}
