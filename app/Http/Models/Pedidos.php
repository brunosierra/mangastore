<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    // Tabela anexa
    protected $table = 'pedido';
    
    /**
     * Relacionamento hasOne
     * pedido_endereco_entrega
     */
    public function entrega(){
        return $this->belongsTo('App\Http\Models\Entregas', 'id', 'id_pedido');
    }
    
    /**
     * Relacionamento hasMany
     * pedido_itens
     */
    public function itens(){
        return $this->hasMany('App\Http\Models\Itens', 'id_pedido', 'id');
    }
}
