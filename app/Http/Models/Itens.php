<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Itens extends Model
{
    // Tabela anexa
    protected $table = 'pedido_itens';
    
    /**
     * Relacionamento hasOne
     * pedido_endereco_entrega
     */
    public function entrega(){
        return $this->belongsTo('App\Http\Models\Pedidos', 'id_pedido', 'id');
    }
    
    /**
     * Relacionamento hasOne
     * produto
     */
    public function produto(){
        return $this->belongsTo('App\Http\Models\Produtofilhos', 'id_produto', 'id');
    }
}
